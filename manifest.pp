$svc_conf_template = @(END)
[Unit]
Description=API para Investimentos

[Service]
User=root
WorkingDirectory=/home/ubuntu/
ExecStart=/usr/bin/java -Xmx512m -jar Api-Investimentos-0.0.1-SNAPSHOT.jar --server.port=8080

[Install]
WantedBy=multi-user.target
END

class {  'java':
       package => 'openjdk-8-jdk',
}

file { '/etc/systemd/system/api-investimento.service':
      ensure  => file,
      content => inline_epp($svc_conf_template),
}

file {'/etc/nginx/conf.d/default.conf':
      ensure => absent,
      notify => Service['nginx'],
}

service { 'api-investimento':
      ensure => running,
}

class{ 'nginx': }

nginx::resource::server { 'angela.host.com':
  listen_port => 80,
  server_name => ["_"],
  proxy       => 'http://localhost:8080',
}

