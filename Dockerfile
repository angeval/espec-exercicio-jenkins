FROM ubuntu:latest
RUN apt-get update && apt-get install -y openjdk-8-jdk
WORKDIR ./app
COPY target/Api*jar ./Api-Investimentos.jar
COPY Dockerfile .
CMD ["java", "-Dserver.port=8080", "-jar", "Api-Investimentos.jar"] 
EXPOSE 8080/tcp